from rest_framework import serializers
from .models import User, Merchant, Organization, Donation

class UserSerializer(serializers.ModelSerializer):
    accounts = serializers.SerializerMethodField()

    def get_accounts(self, model_instance):
        return "accounts_to_add"

    class Meta:
        model = User
        fields = (
            "userId",
            "username",
            "accounts",
            "fullName"
        )

class MerchantSerializer(serializers.ModelSerializer):
    organization = serializers.SerializerMethodField()

    def get_organization(self, model_instance):
        return "organization_to_add"

    class Meta:
        model = Merchant
        fields = (
            "merchantId",
            "displayName",
            "thumbnailURL",
            "donatedBy",
            "organization"
        )

class OrganizationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Organization
        fields = (
            "organizationId",
            "displayName",
            "thumbnailURL",
            "donatedTo",
            "acronym"
        )

class DonationSerializer(serializers.ModelSerializer):
    class Meta:
        merchant = serializers.SerializerMethodField()
        organization = serializers.SerializerMethodField()

        def get_merchant(self, model_instance):
            return "merchant_to_add"

        def get_organization(self, model_instance):
            return "organization_to_add"

        model = Donation
        fields = (
            "donationId",
            "isCompleted",
            "amount",
            "merchant",
            "organization",
            "creationDate"
        )
