from . import views
from django.conf.urls import url
from rest_framework.urlpatterns import format_suffix_patterns

urlpatterns = [
	url(r'^transactions/', views.get_transactions),
    url(r'^user/(?P<primKey>.+)/', views.UserDetail.as_view()),
    url(r'^users/', views.UserList.as_view()),
    url(r'^merchant/(?P<primKey>.+)/', views.MerchantDetail.as_view()),
    url(r'^merchants/', views.MerchantList.as_view()),
    url(r'^organization/(?P<primKey>.+)/', views.OrganizationDetail.as_view()),
    url(r'^organizations/', views.OrganizationList.as_view()),
    url(r'^donation/(?P<primKey>.+)/', views.DonationDetail.as_view()),
    url(r'^donations/', views.DonationList.as_view()),
]

urlpatterns = format_suffix_patterns(urlpatterns)