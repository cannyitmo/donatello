from django.db import models
import uuid
from datetime import date

class User(models.Model):
	userId = models.UUIDField(primary_key = True, default = uuid.uuid4, editable = False, blank = True)
	username = models.CharField(max_length = 100, unique=True)
	fullName = models.CharField(max_length = 100)
	_password = models.CharField(max_length = 100)
	deviceToken = models.CharField(max_length = 50)
	userBankAccounts = models.TextField(blank = True, default = "")

	def __str__(self):
		return str(self.userId)

class Merchant(models.Model):
	merchantId = models.UUIDField(primary_key = True, default = uuid.uuid4, editable = False, blank = True)
	displayName = models.CharField(max_length = 100)
	thumbnailURL = models.URLField(max_length = 200)
	toBeDonated = models.DecimalField(max_digits = 50, decimal_places = 2)
	donatedBy = models.DecimalField(max_digits = 50,decimal_places = 2)
	organizationId = models.CharField(editable = False, max_length = 100)

	def __str__(self):
		return str(self.merchantId)

class Organization(models.Model):
	organizationId = models.UUIDField(primary_key = True, default = uuid.uuid4, editable = False, blank = True)
	displayName = models.CharField(max_length = 100)
	acronym = models.CharField(max_length = 50)
	thumbnailURL = models.URLField(max_length = 200)
	donatedTo = models.DecimalField(max_digits = 50, decimal_places = 2)
	merchantIds = models.TextField()
	organizationBankAccounts = models.TextField()

	def __str__(self):
		return str(self.organizationId)

class Donation(models.Model):
	donationId = models.UUIDField(primary_key = True, default = uuid.uuid4, editable = False, blank = True)
	isCompleted = models.BooleanField()
	amount = models.DecimalField(max_digits = 50, decimal_places = 2)
	userId = models.CharField(editable = False, max_length = 100)
	merchantId = models.CharField(editable = False, max_length = 100)
	organizationId = models.CharField(editable = False, max_length = 100)
	creationDate = models.DateField(default = date.today)

	def __str__(self):
		return str(self.donationId)