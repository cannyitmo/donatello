from django.contrib import admin
from backend import models

admin.site.register(models.User)
admin.site.register(models.Merchant)
admin.site.register(models.Organization)
admin.site.register(models.Donation)