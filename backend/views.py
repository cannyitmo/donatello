from django.http import Http404
from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from .models import User, Merchant, Organization, Donation
from .serializer import UserSerializer, MerchantSerializer, OrganizationSerializer, DonationSerializer
import requests
import datetime
import time
import json as JSON
from django.http import JsonResponse

# Lists are for testing only !!!
# delete mapping from urls as well
class UserList(APIView):
    def get(self, request):
        users = User.objects.all()
        serializer = UserSerializer(users, many=True)
        return Response(serializer.data)

class MerchantList(APIView):
    def get(self, request):
        merchants = Merchant.objects.all()
        serializer = MerchantSerializer(merchants, many=True)
        return Response(serializer.data)

class OrganizationList(APIView):
    def get(self, request):
        organizations = Organization.objects.all()
        serializer = OrganizationSerializer(organizations, many=True)
        return Response(serializer.data)

class DonationList(APIView):
    def get(self, request):
        donations = Donation.objects.all()
        serializer = DonationSerializer(donations, many=True)
        return Response(serializer.data)

class UserDetail(APIView):
    def get_user(self, primaryKey):
        try:
            return User.objects.get(pk=primaryKey)
        except User.DoesNotExist:
            raise Http404

    def get(self, request, primKey):
        snippet = self.get_user(primKey)
        serializer = UserSerializer(snippet)
        return Response(serializer.data)

class MerchantDetail(APIView):
    def get_merchant(self, primaryKey):
        try:
            return Merchant.objects.get(pk=primaryKey)
        except Merchant.DoesNotExist:
            raise Http404

    def get(self, request, primKey):
        snippet = self.get_merchant(primKey)
        serializer = MerchantSerializer(snippet)
        return Response(serializer.data)

class OrganizationDetail(APIView):
    def get_organization(self, primaryKey):
        try:
            return Organization.objects.get(pk=primaryKey)
        except Organization.DoesNotExist:
            raise Http404

    def get(self, request, primKey):
        snippet = self.get_organization(primKey)
        serializer = OrganizationSerializer(snippet)
        return Response(serializer.data)

class DonationDetail(APIView):
    def get_donation(self, primaryKey):
        try:
            return Donation.objects.get(pk=primaryKey)
        except Donation.DoesNotExist:
            raise Http404

    def get(self, request, primKey):
        snippet = self.get_donation(primKey)
        serializer = DonationSerializer(snippet)
        return Response(serializer.data)

# THIS IS FOR TESTING
def get_transactions(request):
    user_bank_accounts = []
    users = User.objects.all()
    for user in users:
        try:
            accounts = user.userBankAccounts.split(',')
            for i in range(len(accounts)):
                user_bank_accounts.append(accounts[i])
        except:
            user_bank_accounts.append(user.userBankAccounts)
    common_url='https://api.hackathon.developer.nordeaopenbanking.com/v2/accounts/'
    # from_date = str(datetime.datetime.now().year) + "-" + str(datetime.datetime.now().month) + "-" + str(datetime.datetime.now().day)
    # to_date = from_date
    from_date = "2016-09-09"
    to_date = "2017-11-25"
    # for j in range(len(user_bank_accounts)):
    url = common_url + "FI6593857450293470-EUR" + "/transactions?fromDate=" + from_date + "&toDate=" + to_date
    headers = {'Authorization':'Bearer eyJjdHkiOiJKV1QiLCJlbmMiOiJBMTI4R0NNIiwiYWxnIjoiZGlyIn0..B2bGCDpJYcDrmdaD.iB2TffgsLBzpb6_UGAbyYe0DiXdpV0SmcMEQOm8MrWLarJKqM_bEOBuZ7O-Qd2jiuc2rRrZiwhWTxEOSONslm-NRMAIJu6Q6ncvXhET_9lNVANSuKXt9XK7WtikI55DkiMQVm2-du1Rh6s6-2Cm5_v0p45-nKx6f5WZ1ZZZN7SkeXFSDSswDR174gSdI29lIJCkfmdGv0VB85JyALohC9hUs91_LUil-F8dMEeFaorm8RjjKQmb53hyuVDJpbZjAoK1lzCrDVOJAsORezxQJcv2eKq8Is78cqOjU5anqiZpQjZwv8wv-GtVtAW3asfnVNdOuYRQEGohRrhHTDVCSoXiXI5fYOpZB4XnlVxhTy5iY5vGNKWk3OJSuOHU0ExryTGYbFfUOQZDptStvk_RGdmO_O5UjuyYAFb-NrHfXVwLBNH6IZdMMm1L4SRj3SNOxbuW91HvlkaYh5BJMALi_sw4UFb61HLQON481Xyx1_xL_N2Ad1qHgAO1iSR51gC3TNoTMtsJVcPZtzHwvnrOjDcEC9hfcWMgFTMd2nGUfXEarT2gV9JJFigpZbc0OAJypqMj7wapMGSG7sgV8u_A2cgUg___dChTgp2Y4FMUYfsQWra1PVSvaW4IkREqVTXTZmG8qrHbrI2bclsz9ReiTOw4aWMtqNGjxJBdUEOZDu0q0aLxweo9k60h6SRyO3DshXkZH-b_JseAWFoiPivlvr7f_iV1BS6aqGS1jjwp4iTWVa7NKu9uf0k2v9jlv07AfjtsiRoXxdJ-pxp1q9XXxHaf9JLiJf-U0MeTrr1zvX-UxFvgLz0dVb51NRCekM-02d3tBxpM7tY0bWrxHrVfWxoJVQociDMx7rgZ3plHytw2W95gNhOasiGlK5ACfHVTpruN1K285FqTldQoUwPYJ9vWX5sgmlQQI9QRKzuNbcfI-aK09do9eL8kHpw0tiJ-dBMd3Qg9sxa7-Fj_oR97eQ7HmcylhbzmI_krFIAEj0Sf1ptw2JHCFC4ZnsZtYt0Y77ytRKtb1GN0uvw0GPDg8b2R91jm5mp5Q5PNrE3fHBQQZgyq3eSe-tzwFEs2aHwHmybb_0P_RyDW4be207ysXYOZ3BO_oAg6cs1De-8Qix6_Z6gwitqPR0pn0NDukyuw52aYf_5_FY2mC-PwEogb4o3obd4tJzNfXTp7LNNP6xyJNbIVWtuvUF-E-6OgaMPcvDxmWU9riPugOvHScEhJCsj7ZaO0P2HwoK5q6dgh44Tcynz-JyaATm7xoQ34cdqneDpN8hF_ZlKH-Xj4QmgOA.Ytm-evs7pLXzjklbx6c7uw', 
                    'Content-Type':'application/json','X-IBM-Client-Id':'b7f5821e-3108-4d63-a7d4-26d06bf4d091','X-IBM-Client-Secret':'kX7qM4dS3mX6lA2iS1lW6wB5eW3wQ0lQ5pC6aY3bU4cF6nL1xX'}
    response = JSON.loads(requests.get(url, headers = headers).text)
    result = {}
    for t in range(len(response['response']['transactions'])):
        res = {}
        if(response['response']['transactions'][t]['_type']=="CreditTransaction"):
            res['transactionId'] = response['response']['transactions'][t]['transactionId']
            res['amount'] = response['response']['transactions'][t]['amount']
            res['debtorName'] = response['response']['transactions'][t]['debtorName']
            result.setdefault('transactions',[]).append(res)
    return JsonResponse(result)

# THIS WILL BE IN THE PROJECT

# def get_transactions(request):
#     previous_result = {'transactions':[{'transactionId':'', 'debtorName':'', 'amount':''}]}
#     while True:
#         users = User.objects.all()
#         for user in users:
#             try:
#                 accounts = user.userBankAccounts.split(',')
#                 for i in range(len(accounts)):
#                     user_bank_accounts.append(accounts[i])
#             except:
#                 user_bank_accounts.append(user.userBankAccounts)
#         common_url='https://api.hackathon.developer.nordeaopenbanking.com/v2/accounts/'
#         from_date = str(datetime.datetime.now().year) + "-" + str(datetime.datetime.now().month) + "-" + str(datetime.datetime.now().day)
#         to_date = from_date
#         from_date = "2016-09-09"
#         to_date = "2017-11-25"
#         for j in range(len(user_bank_accounts)):
#             url = common_url + user_bank_accounts[t] + "/transactions?fromDate=" + from_date + "&toDate=" + to_date
#             headers = {'Authorization':'Bearer eyJjdHkiOiJKV1QiLCJlbmMiOiJBMTI4R0NNIiwiYWxnIjoiZGlyIn0..B2bGCDpJYcDrmdaD.iB2TffgsLBzpb6_UGAbyYe0DiXdpV0SmcMEQOm8MrWLarJKqM_bEOBuZ7O-Qd2jiuc2rRrZiwhWTxEOSONslm-NRMAIJu6Q6ncvXhET_9lNVANSuKXt9XK7WtikI55DkiMQVm2-du1Rh6s6-2Cm5_v0p45-nKx6f5WZ1ZZZN7SkeXFSDSswDR174gSdI29lIJCkfmdGv0VB85JyALohC9hUs91_LUil-F8dMEeFaorm8RjjKQmb53hyuVDJpbZjAoK1lzCrDVOJAsORezxQJcv2eKq8Is78cqOjU5anqiZpQjZwv8wv-GtVtAW3asfnVNdOuYRQEGohRrhHTDVCSoXiXI5fYOpZB4XnlVxhTy5iY5vGNKWk3OJSuOHU0ExryTGYbFfUOQZDptStvk_RGdmO_O5UjuyYAFb-NrHfXVwLBNH6IZdMMm1L4SRj3SNOxbuW91HvlkaYh5BJMALi_sw4UFb61HLQON481Xyx1_xL_N2Ad1qHgAO1iSR51gC3TNoTMtsJVcPZtzHwvnrOjDcEC9hfcWMgFTMd2nGUfXEarT2gV9JJFigpZbc0OAJypqMj7wapMGSG7sgV8u_A2cgUg___dChTgp2Y4FMUYfsQWra1PVSvaW4IkREqVTXTZmG8qrHbrI2bclsz9ReiTOw4aWMtqNGjxJBdUEOZDu0q0aLxweo9k60h6SRyO3DshXkZH-b_JseAWFoiPivlvr7f_iV1BS6aqGS1jjwp4iTWVa7NKu9uf0k2v9jlv07AfjtsiRoXxdJ-pxp1q9XXxHaf9JLiJf-U0MeTrr1zvX-UxFvgLz0dVb51NRCekM-02d3tBxpM7tY0bWrxHrVfWxoJVQociDMx7rgZ3plHytw2W95gNhOasiGlK5ACfHVTpruN1K285FqTldQoUwPYJ9vWX5sgmlQQI9QRKzuNbcfI-aK09do9eL8kHpw0tiJ-dBMd3Qg9sxa7-Fj_oR97eQ7HmcylhbzmI_krFIAEj0Sf1ptw2JHCFC4ZnsZtYt0Y77ytRKtb1GN0uvw0GPDg8b2R91jm5mp5Q5PNrE3fHBQQZgyq3eSe-tzwFEs2aHwHmybb_0P_RyDW4be207ysXYOZ3BO_oAg6cs1De-8Qix6_Z6gwitqPR0pn0NDukyuw52aYf_5_FY2mC-PwEogb4o3obd4tJzNfXTp7LNNP6xyJNbIVWtuvUF-E-6OgaMPcvDxmWU9riPugOvHScEhJCsj7ZaO0P2HwoK5q6dgh44Tcynz-JyaATm7xoQ34cdqneDpN8hF_ZlKH-Xj4QmgOA.Ytm-evs7pLXzjklbx6c7uw', 
#                             'Content-Type':'application/json','X-IBM-Client-Id':'b7f5821e-3108-4d63-a7d4-26d06bf4d091','X-IBM-Client-Secret':'kX7qM4dS3mX6lA2iS1lW6wB5eW3wQ0lQ5pC6aY3bU4cF6nL1xX'}
#             response = JSON.loads(requests.get(url, headers = headers).text)
#             result = {}
#             for t in range(len(response['response']['transactions'])):
#                 res = {}
#                 if(response['response']['transactions'][t]['_type']=="CreditTransaction"):
#                     res['transactionId'] = response['response']['transactions'][t]['transactionId']
#                     res['amount'] = response['response']['transactions'][t]['amount']
#                     res['debtorName'] = response['response']['transactions'][t]['debtorName']
#                     result.setdefault('transactions',[]).append(res)
#             if(result == previous_result):
#                 pass
#             else:
#                 for s in range(len(result['transactions'])):
#                     for v in range(len(previous_result['transactions'])):
#                         if(result['transactions'][s]['transactionId']!=previous_result['transactions'][v]['transactionId']):
#                             FUNCTION FOR PUSH !!!!!!!!!
#                 previous_result = result
#         time.sleep(60)